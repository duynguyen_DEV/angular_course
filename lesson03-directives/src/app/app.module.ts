import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { StructuralComponent } from "./components/structural/structural.component";
import { AttributeComponentComponent } from "./components/attribute-component/attribute-component.component";

@NgModule({
  declarations: [
    AppComponent,
    StructuralComponent,
    AttributeComponentComponent
  ],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
