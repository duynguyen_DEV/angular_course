import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-attribute",
  templateUrl: "./attribute-component.component.html",
  styleUrls: ["./attribute-component.component.css"]
})
export class AttributeComponentComponent implements OnInit {
  isSpecial = false;

  constructor() {}

  ngOnInit() {}

  onToggle() {
    this.isSpecial = !this.isSpecial;
  }

  setClasses() {
    return { "bd-green": this.isSpecial, "pd-10": this.isSpecial };
  }
}
