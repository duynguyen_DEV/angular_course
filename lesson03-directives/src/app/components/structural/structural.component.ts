import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-structural",
  templateUrl: "./structural.component.html",
  styleUrls: ["./structural.component.css"]
})
export class StructuralComponent implements OnInit {
  isShown: boolean = true;
  userName: string = "NGUYEN";
  valueCheck = false;
  age: number;

  someCities: string[] = [
    "Le Kremlin Bicêtre",
    "Thias",
    "Villejuif",
    "Vitry Sur Seine"
  ];

  products: any[] = [
    {
      id: "i6Pl32",
      name: "iPhone 6 Plus 32GB",
      image:
        "https://technabob.com/blog/wp-content/uploads/2014/09/iphone_6_6_plus.jpg"
    },
    {
      id: "i7Pl32",
      name: "iPhone 7 Plus 32GB",
      image: "http://brain-images.cdn.dixons.com/9/0/10151609/u_10151609.jpg"
    },
    {
      id: "SS1032",
      name: "Samsung S10 32GB",
      image:
        "https://www.hackread.com/wp-content/uploads/2019/03/new-samsung-galaxy-s10-review-and-features-3.png"
    }
  ];

  productsFromServer: any[] = [
    {
      id: "i6Pl32",
      name: "iPhone 6 Plus 32GB",
      image:
        "https://technabob.com/blog/wp-content/uploads/2014/09/iphone_6_6_plus.jpg"
    },
    {
      id: "i7Pl32",
      name: "iPhone 7 Plus 32GB",
      image: "http://brain-images.cdn.dixons.com/9/0/10151609/u_10151609.jpg"
    },
    {
      id: "SS1032",
      name: "Samsung S10 32GB",
      image:
        "https://www.hackread.com/wp-content/uploads/2019/03/new-samsung-galaxy-s10-review-and-features-3.png"
    },
    {
      id: "HWP20",
      name: "Huawei P20 Lite 32GB",
      image:
        "https://d2bq50ryx4ze5e.cloudfront.net/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/p/2/p20-lite-blk-1.jpg"
    },
    {
      id: "OpF11P",
      name: "Oppo F11 Pro 32GB",
      image: "https://mobidevices.ru/images/2019/02/Oppo-F11-Pro.jpg"
    }
  ];

  public users: any[] = [
    {
      name: "Nguyen",
      country: "Vietnam"
    },
    {
      name: "Ba ton quan xa lon",
      country: "Laos"
    },
    {
      name: "Cap dat ma an",
      country: "Thailande"
    },
    {
      name: "Tao Lao",
      country: "Vietnam"
    },
    {
      name: "Bum lum tun",
      country: "Thailande"
    },
    {
      name: "Chot khe lot khe",
      country: "Laos"
    },
    {
      name: "Tran van Truon",
      country: "Vietnam"
    },
    {
      name: "MocCuRaDov",
      country: "Russe"
    },
    {
      name: "Angkovat",
      country: "Cambodge"
    }
  ];

  constructor() {}

  ngOnInit() {}

  onToggle() {
    this.isShown = !this.isShown;
  }

  onChecked(value) {
    this.valueCheck = value;
  }

  loadMoreData() {
    this.products = this.productsFromServer;
  }

  trackByID(index, item) {
    return item.id;
  }
}
